
import org.junit.Assert;
import org.junit.Test;
import org.woo.udf.BSONParserUDF;

public class BSONParserUDFTest {

    @Test
    public void testBSONParserUDF() {

        BSONParserUDF udf = new BSONParserUDF();

        String bsonInput = "666f9ea36291570dba18d395";

        String result = null;
        try {
            result = udf.call(bsonInput);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        Assert.assertEquals("2024-06-17 02:25:39", result);
    }
}