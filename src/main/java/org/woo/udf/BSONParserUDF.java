package org.woo.udf;

import org.apache.spark.sql.api.java.UDF1;

import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class BSONParserUDF implements UDF1<String, String> {
    @Override
    public String call(String objectIdHex) throws Exception {
        try {
            ObjectId objectId = new ObjectId(objectIdHex);

            Date generationTime = objectId.getDate();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));  // 确保使用UTC时区

            return sdf.format(generationTime);
        } catch (Exception e) {
            return "Error: " + e.getMessage();
        }
    }
}